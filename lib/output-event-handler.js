const Matchbox = require('matchbox-js');
module.exports = class EventQueueProviderTest extends Matchbox.QueueBaseProvider {
    constructor(config) {
        super(config);
        if (!this.config.testQueueAccumulate) {
            this.queued = 0;
            return;
        }
        this.accumulated = {};
        this.queued = [];
        this.accumulator = require('./accumulator').create();
    }
    static create(config) {
        return new EventQueueProviderTest(config);
    }
    async startInternal() {
        
    }
    async pushInternal(msg) {
        if (!this.config.testQueueAccumulate) {
            this.queued++;
            return;
        }
        if (msg.eventType === Matchbox.Constants.EVENT_TYPE.TRANSACTION) {
            if (!this.accumulated[msg.payload.transactionType])
                this.accumulated[msg.payload.transactionType] = 0;
            this.accumulated[msg.payload.transactionType]++;
            this.accumulator.add(msg.payload.tokenCode, msg.payload.amount, msg.payload.fee);
        }
        this.queued.push(msg);
    }
}