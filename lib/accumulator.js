const utils = require('matchbox-js').Utils();
module.exports = class Accumulator {
    constructor() {
        this.accumulated = {};
    }
    static create() {
        return new Accumulator();
    }
    getCalculation(token) {
        if (!this.accumulated[token])
            this.accumulated[token] = utils.numbers.chain();
        return this.accumulated[token];
    }
    add(token, amount, fee) {
        const calculation = this.getCalculation(token).add(amount);
        if (fee) calculation.add(fee);
    }
    results() {
        return Object.keys(this.accumulated).map(token => {
            return { token, total: this.accumulated[token].result() }
        });
    }
}