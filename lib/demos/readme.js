const uuid = require('uuid');
const Matchbox = require('matchbox-js');

//first define your output event handler
class EventQueueProviderTest extends Matchbox.QueueBaseProvider {
    constructor(config) {
        super(config);
    }
    static create(config) {
        return new EventQueueProviderTest(config);
    }
    async startInternal() {}
    async pushInternal(msg) {
        let eventName = getName(Matchbox.Constants.EVENT_TYPE, msg.eventType);
        if (eventName === 'TRANSACTION') {
            eventName = `${getName(Matchbox.Constants.ORDERS.CLOSE_TYPE, msg.payload.transactionType)} ${eventName}`;
        }
        log(`${eventName} happened...`,  msg);
    }
}

//then set up your market
const market = Matchbox.Market({}, EventQueueProviderTest);

// add our XRP token
market.addToken(
    Matchbox.TokenBuilder()
        .withCode('XRP') // self explanatory
        .withDescription('Ripple token') //not being actively used yet, but will be in diagnostics
        .withSmallestUnitName('drop') //not being actively used yet, but will be in diagnostics
        .withSmallestUnitScale('1000000') // so everything is perfectly rounded
        .build()
    );
// add our BTC token
market.addToken(
    Matchbox.TokenBuilder()
        .withCode('BTC')
        .withDescription('Bitcoin token')
        .withSmallestUnitName('satoshi')
        .withSmallestUnitScale('100000000')
        .build()
    );
// add our BTC/XRP instrument
market.addInstrument(
    Matchbox.InstrumentBuilder()
        // allowed otrder types (all of them)
        .withAllowedOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withAllowedOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.MARKET)
        .withAllowedOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED)
        // add ourt base token BTC
        .withBaseToken(
            Matchbox.PairItemBuilder(market.getToken('BTC'))
                .withMakerFeePercentage('0.1') //makers are charged per fill
                .withTakerFeePercentage('0.2') //takers are charged per fill
            )
        // add our quote token XRP
        .withQuoteToken(
            Matchbox.PairItemBuilder(market.getToken('XRP'))
                .withMakerFeePercentage('0.1')
                .withTakerFeePercentage('0.2')
            )
    .build()
);

//activate the instrument
market.activateInstrument('BTC/XRP');

//throw in some orders - these should match and output results in EventQueueProviderTest.pushInternal (above)
const account1Id = uuid.v4();
const account2Id = uuid.v4();
const accountSpot1Id = uuid.v4();
const accountSpot2Id = uuid.v4();

market.orderPush(
    Matchbox.OrderBuilder()
      .withOrderId('bid-perfect-match')
      .withTradeAccountId(account1Id)
      .withInstrument('BTC/XRP')
      .withDirection(Matchbox.Constants.ORDERS.DIRECTION.BID)
      .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
      .withQuantity('50')
      .withPrice('2')
      .withSpotAccountId(accountSpot1Id)
      .build()
  );

market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('offer-perfect-match')
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('25')
        .withPrice('2')
        .withSpotAccountId(accountSpot2Id)
        .build()
);

log('the above should match perfectly - the orderbook will be empty', market.getOrderBook('BTC/XRP'));

//throw in another order
market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('offer-unmatched')
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('25')
        .withPrice('3')
        .withSpotAccountId(accountSpot2Id)
        .build()
);

log('list orders by the tradeAccountId - array should only contain last order', market.getOrdersByTradeAccountId('BTC/XRP', account2Id));
log('list orders by the spotAccountId - array should only contain last order', market.getOrdersBySpotAccountId('BTC/XRP', accountSpot2Id));
log('orderbook only contains last offer quantity', market.getOrderBook('BTC/XRP'));

//throw in another limit bid order
market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('bid-unmatched')
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.BID)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('100')
        .withPrice('2')
        .withSpotAccountId(accountSpot1Id)
        .build()
);

log('orderbook contains orders on both sides', market.getOrderBook('BTC/XRP'));

//throw in a mid-point-pegged order
market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('bid-mid-point-pegged')
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.BID)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED)
        .withQuantity('50')
        .withSpotAccountId(accountSpot2Id)
        .build()
);

//throw in a market order - should match the mid point pegged order at a price of 2.5
//this means the mid point pegged gobbles up 20 BTC, then the bid--unmatched limit gobbles up the other 30 @ 2 - so we have 40 left over on bid-unmatched
market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('offer-market')
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.MARKET)
        .withQuantity('50')
        .withSpotAccountId(accountSpot1Id)
        .build()
);

log('orderbook contains orders on both sides, bid-unmatched has 40.00 left over (60.00 was filled by the market offer after bid-mid-point-pegged)', market.getOrderBook('BTC/XRP'));

//throw in a limit order with a fill or kill rule - the order will be refunded immediatley if it cannot be filled completely by another order
market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('offer-fill-or-kill')
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('1000')
        .withPrice('2')
        .withSpotAccountId(accountSpot2Id)
        .withFillOrKill(true) // causes this order to be cancelled and refunded as there is nothing at the top of the order book to match it completely
        .build()
);

//amend the offer order so it matches with the bid order
market.orderAmend('offer-unmatched', { price: 2 })

log('orderbook is now emptied on the bid side, just 5 BTC left at price of 2 XRP to BTC', market.getOrderBook('BTC/XRP'));

//close off the market by adding a final bid that perfectly fills the remaining offer
market.orderPush(
    Matchbox.OrderBuilder()
        .withOrderId('bid-close')
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.BID)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('10')
        .withPrice('2')
        .withSpotAccountId(accountSpot1Id)
        .build()
);

log('orderbook is now empty', market.getOrderBook('BTC/XRP'));

// utilty methods: 

//logging utility method
function log (msg, data) {
    console.log('\r\n');
    console.log(msg);
    console.log(new Array(msg.length).fill('-').join(''));
    console.log(JSON.stringify(data, null, 2));
}

//utility for making enumerations human-readable
function getName(enumeration, val) {
    return Object.keys(enumeration).find((key, ind) => {
        if (Object.values(enumeration)[ind] === val) return key;
        return false;
    });
}
