const Matchbox = require('matchbox-js');
const PERIOD = 5000;
const utils = Matchbox.Utils();
const uuid = require('uuid');
const delay = require('await-delay');

async function run() {
  const config = {
    eventQueue: {
      testQueueAccumulate: false // we  are not wanting to store anything for future inspection
    }
  };
  const eventQueueProviderImpl = require('../output-event-handler');

  const market = Matchbox.Market(config, eventQueueProviderImpl);
  await setUpMarket(market);
  await market.start();

  const account1Id = uuid.v4();
  const account2Id = uuid.v4();
  const accountSpot1Id = uuid.v4();
  const accountSpot2Id = uuid.v4();

  console.log('building orders...');
  let orders = buildOrders(account1Id, accountSpot1Id, account2Id, accountSpot2Id);
  console.log('orders built...');

  let matching = true;
  let started = Date.now();
  let orderCount = 0;

  setTimeout(async () => {
    matching = false;
    let actualPeriod = Date.now() - started;
    console.log(market.getOrderBook('BTC/XRP'));
    console.log('BENCHMARK SUCCESSFULL');
    console.log('---------------------');
    console.log(
      `outputted ${
        market.eventQueueProvider.queued
      } fill transactions in ${actualPeriod}ms, ${(orderCount / actualPeriod) *
        1000} matches per second`
    );
  }, PERIOD);

  while (matching) {
    market.orderPush(orders.pop());
    orderCount++;
    // await delay(100);
    // console.log(`pushed ${orderCount}`);
    if (orderCount % 1000 === 0) {
      console.log(`pushed ${orderCount} orders`);
      // console.log('order book', market.getOrderBook('BTC/XRP'));
      // console.log('persisted data', market.activePersistenceProvider.data);
      await delay(1);
    }
  }
}

function buildOrders(account1Id, accountSpot1Id, account2Id, accountSpot2Id) {
  const orders = [];
  for (var i = 0; i < 100000; i++) {
    let bidQuantity = `${getRandomInt(1, 1000)}.${getRandomInt(1, 1000)}`;
    let price = `${getRandomInt(1, 2)}.${getRandomInt(1, 20)}`;
    let offerQuantity = utils.numbers
      .chain(bidQuantity)
      .divide(price)
      .truncate(8)
      .result();

    orders.push(
      Matchbox.OrderBuilder()
        .withOrderId(uuid.v4())
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.BID)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(bidQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot1Id)
        .build()
    );

    orders.push(
      Matchbox.OrderBuilder()
        .withOrderId(uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(Matchbox.Constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(offerQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );
  }
  return orders;
}

async function setUpMarket(market) {
  market.addToken(
    Matchbox.TokenBuilder()
      .withCode('XRP')
      .withDescription('Ripple token')
      .withSmallestUnitName('drop')
      .withSmallestUnitScale('1000000')
      .build()
  );
  market.addToken(
    Matchbox.TokenBuilder()
      .withCode('BTC')
      .withDescription('Bitcoin token')
      .withSmallestUnitName('satoshi')
      .withSmallestUnitScale('100000000')
      .build()
  );
  market.addInstrument(
    Matchbox.InstrumentBuilder()
      .withAllowedOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.LIMIT)
      .withAllowedOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.MARKET)
      .withAllowedOrderType(Matchbox.Constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED)
      .withBaseToken(
        Matchbox.PairItemBuilder(market.getToken('BTC'))
          .withMakerFeePercentage('0.1')
          .withTakerFeePercentage('0.2')
      )
      .withQuoteToken(
        Matchbox.PairItemBuilder(market.getToken('XRP'))
          .withMakerFeePercentage('0.1')
          .withTakerFeePercentage('0.2')
      )
      .build()
  );
  market.activateInstrument('BTC/XRP');
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

run();
